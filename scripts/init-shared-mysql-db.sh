#!/bin/sh

# fail on errors
set -e

if [ ! -f docker-compose.yml ]; then
    echo "You are not inside sh-stack. The working directory must be the root of sh-stack."
    exit 1
fi

MAX_WAIT=360 # max. 5 minute waiting time in loop before timeout

docker-compose up -d sh-shared-mysql-db
docker-compose logs -f sh-shared-mysql-db &
log_process_id=$!

echo "Waiting for sh-shared-mysql-db"
current_wait=0
while ! docker exec -i sh-shared-mysql-db sh -c "mysqladmin ping -h 127.0.0.1 -uroot -papple" >/dev/null 2>&1
do
  if [ ${current_wait} -ge ${MAX_WAIT} ]; then
    echo "Timeout on startup of sh-core-db"
    kill -TERM ${log_process_id}
    exit 1
  fi
  current_wait=$((current_wait+1))
  sleep 1
done

kill -TERM ${log_process_id}

. config/sh-shared-mysql-db/sh-shared-mysql-db.env

create() {
  database=$1
  username=$2
  password=$3
  db_options=${4:-}

  echo "Creating user ${username} with database ${database}"
  docker exec -i sh-shared-mysql-db mysql --user=root <<SQL_SCRIPT
    CREATE DATABASE IF NOT EXISTS \`${database}\` ${db_options};
    CREATE USER '${username}'@'%' IDENTIFIED BY '${password}';
    GRANT ALL PRIVILEGES ON \`${database}\`.* TO '${username}'@'%';
SQL_SCRIPT
}

create "sh-anope" "sh-anope" "${MYSQL_ANOPE_PASSWORD}"
create "sh-anope" "sh-java-api" "${MYSQL_JAVA_API_PASSWORD}"
create "sh-mautic" "sh-mautic" "${MYSQL_MAUTIC_PASSWORD}" "CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci"
create "sh-postal" "sh-postal" "${MYSQL_POSTAL_PASSWORD}" "CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci"
#create "sh-wiki" "sh-wiki" "${WIKI_DB_PASSWORD}"
#create "sh-wordpress" "sh-wordpress" "${WORDPRESS_DB_PASSWORD}"
#create "sh-phpbb3" "sh-phpbb3" "${PHPBB3_DB_PASSWORD}"

echo "Giving sh-postal privileges for additional prefixed databases"
docker exec -i sh-shared-mysql-db mysql --user=root <<SQL_SCRIPT
    GRANT ALL PRIVILEGES ON \`${POSTAL_MESSAGE_DATABASE_PREFIX}-%\`.* to 'sh-postal'@'%';
SQL_SCRIPT

## To update the IRC password, we give the server/api full bloated access to all of anope's tables.
#docker exec -i sh-shared-mysql-db mysql --user=root <<SQL_SCRIPT
#    GRANT ALL PRIVILEGES ON "sh-anope".* TO "sh-java-api"@"%";
#SQL_SCRIPT
