#!/bin/sh

# fail on errors
set -e

if [ ! -f docker-compose.yml ]; then
    echo "You are not inside sh-stack. The working directory must be the root of sh-stack."
    exit 1
fi


if [ -d "./data/sh-nodebb" ]; then
    echo "sh-nodebb directory already exists! Installation aborted."
    exit 1
fi

#docker-compose run --rm sh-nodebb sh -c "npm install && npm cache clean --force"
docker-compose run --rm sh-nodebb sh -c "./nodebb setup"

echo "sh-nodebb setup done! Don't forget to write down the admin accounts password!"
