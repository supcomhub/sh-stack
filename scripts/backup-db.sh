#!/bin/sh

BACKUP_DIR=/opt/sh/backups/sh-core-db

mkdir -p "${BACKUP_DIR}"
# FIXME postgres
docker exec -i sh-core-db mysqldump --login-path=faf_lobby --single-transaction --triggers --routines --all-databases | gzip -c > "${BACKUP_DIR}/$(date +"%Y-%m-%d-%H-%M-%S").sql.gzip"
