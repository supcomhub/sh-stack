#!/bin/sh

# fail on errors
set -e

if [ ! -f docker-compose.yml ]; then
    echo "You are not inside sh-stack. The working directory must be the root of sh-stack."
    exit 1
fi

MAX_WAIT=360 # max. 5 minute waiting time in loop before timeout

docker-compose up -d sh-core-db
docker-compose logs -f sh-core-db &
log_process_id=$!

echo "Waiting for sh-core-db"
current_wait=0
while ! docker exec -i sh-core-db sh -c "pg_isready -U postgres" >/dev/null 2>&1
do
  if [ ${current_wait} -ge ${MAX_WAIT} ]; then
    echo "Timeout on startup of sh-core-db"
    kill -TERM ${log_process_id}
    exit 1
  fi
  current_wait=$((current_wait+1))
  sleep 1
done

kill -TERM ${log_process_id}

echo "Waiting for sh-core-db-migrations"
docker-compose run --rm sh-core-db-migrations migrate || { echo "Failed migrate database"; exit 1; }

. config/sh-core-db/sh-core-db.env

create() {
  database=$1
  username=$2
  password=$3
  # db_options=${4:-}

  echo "Creating user ${username} with database ${database}"
  docker exec -i sh-core-db psql -U postgres "${database}" <<SQL_SCRIPT
    CREATE USER "${username}" WITH ENCRYPTED PASSWORD '${password}';
    GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA "public" TO "${username}";
    ALTER DEFAULT PRIVILEGES IN SCHEMA "public" GRANT ALL ON TABLES TO "${username}";
    ALTER DEFAULT PRIVILEGES IN SCHEMA "public" GRANT USAGE  ON SEQUENCES TO "${username}";
    GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA "public" TO "${username}";
SQL_SCRIPT
}

create "${POSTGRES_DB}" "sh-java-api" "${JAVA_API_DB_PASSWORD}"
create "${POSTGRES_DB}" "sh-aio-replayserver" "${AIO_REPLAYSERVER_DB_PASSWORD}"
create "${POSTGRES_DB}" "sh-java-server" "${JAVA_SERVER_DB_PASSWORD}"


## Allows sh-mysql-exporter to read metrics. It is recommended to set a max connection limit for the user to avoid
## overloading the server with monitoring scrapes under heavy load.
#docker exec -i sh-core-db mysql --user=root --password=${ROOT_DB_PASSWORD} <<SQL_SCRIPT
#  CREATE USER 'sh-mysql-exporter'@'%' IDENTIFIED BY '${MYSQL_EXPORTER_DB_PASSWORD}' WITH MAX_USER_CONNECTIONS 3;
#  GRANT PROCESS, REPLICATION CLIENT, SELECT ON *.* TO 'sh-mysql-exporter'@'%';
#SQL_SCRIPT
