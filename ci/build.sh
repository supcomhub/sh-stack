#!/bin/sh

if [ -z $CI ]; then
    echo "This script is only intended for Gitlab CI use. Do not run it on production. IT WILL DELETE YOUR DATABASE."
    exit 1
fi

echo "$CI_JOB_TOKEN" | docker login -u gitlab-ci-token registry.gitlab.com --password-stdin

cp -r config.template config
cp .env.template .env

scripts/init-core-db.sh
scripts/init-shared-mysql-db.sh

MAX_WAIT=360 # max. 5 minute waiting time in loop before timeout

docker-compose up -d sh-java-api
docker-compose logs -f sh-java-api &
log_process_id=$!

echo "Waiting for sh-java-api"
current_wait=0
while ! curl -s --max-time 1 http://docker:8010 > /dev/null 2>&1
do
  if [ ${current_wait} -ge ${MAX_WAIT} ]; then
    echo "Timeout on startup of sh-java-api"
    kill -TERM ${log_process_id}
    exit 1
  fi
  current_wait=$((current_wait+1))
  sleep 1
done

kill -TERM ${log_process_id}

echo "Creating test-data in sh-core-db"
docker-compose run --rm --entrypoint /bin/sh sh-core-db-migrations -c "cat /test-data.sql" > ./test-data.sql
docker exec -i sh-core-db /bin/sh -c "psql sh -v ON_ERROR_STOP=1 -U postgres" < ./test-data.sql > /dev/null

#docker-compose up -d sh-website
#docker-compose logs -f sh-website &
#log_process_id=$!
#
#echo "Waiting for sh-website"
#current_wait=0
#while ! curl -s --max-time 1 http://docker:8020 >/dev/null 2>&1
#do
#  if [ ${current_wait} -ge ${MAX_WAIT} ]; then
#    echo "Timeout on startup of sh-website"
#    kill -TERM ${log_process_id}
#    exit 1
#  fi
#  current_wait=$((current_wait+1))
#  sleep 1
#done
#
#kill -TERM ${log_process_id}
#
#echo "Loading test data"
#docker exec -i sh-core-db psql -U postgres sh < test-data.sql
#run test collection
#docker run --network="host" -t -v ./tests:/tests postman/newman_ubuntu1404 run "file:///tests/postman-collection.json"
